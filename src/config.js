// TODO: Make env to be more configurable via Render
export const API_PROTOCOL = process.env.REACT_APP_API_PROTOCOL || "http";
export const API_HOSTNAME = process.env.REACT_APP_API_HOSTNAME || "localhost";
export const API_PORT = process.env.REACT_APP_API_PORT || "4000";
export const UNIQUE_INITIAL_ADMIN_ID =
  process.env.REACT_APP_UNIQUE_INITIAL_ADMIN_ID || "InitialAdminIdNotSet";

export const apiBaseUrl = `${API_PROTOCOL}://${API_HOSTNAME}:${API_PORT}`;
